package com.example.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import com.example.test.controllers.HelloController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestApplicationTests {

	@Autowired HelloController helloController;

	@Test
	void contextLoads() throws Exception {
		assertNotNull(helloController);
	}

}
