package com.example.test.application;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloApplication {
    
    public String greeting(String name) {
        return "Hello " + name;
    }
}
