package com.example.test.controllers;

import com.example.test.application.HelloApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
    
    @Autowired HelloApplication helloApplication;

    @RequestMapping("/")
	public @ResponseBody String greeting(@RequestParam String name) {
		return helloApplication.greeting(name);
	}
    
}
